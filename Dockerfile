FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build

WORKDIR /opt/leftist_support

ENV ANDROID_SDK_ROOT=/opt/android-sdk-linux

# JAVA
RUN apt-get update && \
    apt-get install -y openjdk-11-jdk && \
    rm -rf /var/lib/apt/lists/*

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/

# Install workload maui
RUN dotnet workload install maui-android --ignore-failed-sources && \
    # delete packages to save space that we don't actually need to build for .net 7.0
    find /usr/share/dotnet/packs/ -type d \( -name "6.0.*" -o -name "32.0.*" \) -maxdepth 2 -exec rm -rf {} \;

# Utils
RUN apt-get update && apt-get install -y \
    unzip && \
    rm -rf /var/lib/apt/lists/*

# Install Android SDK
RUN mkdir -p $ANDROID_SDK_ROOT/cmdline-tools/latest && \
    # https://developer.android.com/studio#command-tools
    curl -k "https://dl.google.com/android/repository/commandlinetools-linux-9477386_latest.zip" -o commandlinetools-linux.zip && \
    unzip -q commandlinetools-linux.zip -d $ANDROID_SDK_ROOT/tmp && \
    mv  $ANDROID_SDK_ROOT/tmp/cmdline-tools/* $ANDROID_SDK_ROOT/cmdline-tools/latest && \
    rm -rf $ANDROID_SDK_ROOT/tmp/ && \
    rm commandlinetools-linux.zip 

ENV PATH=$ANDROID_SDK_ROOT/cmdline-tools/latest/bin:$PATH

RUN yes | sdkmanager --licenses && \
    sdkmanager "platform-tools" && \
    sdkmanager "build-tools;33.0.0" "platforms;android-33"

# Install T4 util for build-time templates generation, see https://github.com/mono/t4
ENV DOTNET_TOOL_PATH=/opt/dotnet_tools
RUN dotnet tool install --tool-path $DOTNET_TOOL_PATH dotnet-t4
ENV PATH=$DOTNET_TOOL_PATH:$PATH

# ------------------------------------
# all lines below are for local build:
# ------------------------------------
#COPY src/LeftistSupport.sln src/
#COPY src/LeftistSupport/LeftistSupport.csproj src/LeftistSupport/
#COPY src/LeftistSupport.Logic/LeftistSupport.Logic.csproj src/LeftistSupport.Logic/
#COPY src/LeftistSupport.Tests/LeftistSupport.Tests.csproj src/LeftistSupport.Tests/
#
#RUN cd src && dotnet restore && cd ..
#
#COPY .git .git/
#COPY src src/
#
#RUN cd src && \
#    dotnet build -f net7.0-android -c Release /p:AndroidSdkDirectory=$ANDROID_SDK_ROOT
