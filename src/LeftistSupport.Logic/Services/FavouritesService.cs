﻿using LeftistSupport.Logic.Models;
using Newtonsoft.Json;
using NLog;

namespace LeftistSupport.Logic.Services
{
    public sealed class FavouritesService
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        private readonly IComradeProvider m_provider;

        public FavouritesService(IComradeProvider provider)
        {
            m_provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        public async Task<IList<FavouriteComrade>> GetFavouriteComradesAsync()
        {
            var favJson = await m_provider.GetFavouritesAsync();

            var settings = new JsonSerializerSettings();
            settings.Error += OnJsonDeserializeError;

            try
            {
                return JsonConvert.DeserializeObject<IList<FavouriteComrade>>(favJson, settings);
            }
            catch (Exception ex)
            {
                m_logger.ForExceptionEvent(ex).Message("Failed to deserialize favourites").Log();
                throw new AggregateException("Bad input", ex);
            }
        }

        public async Task SetFavouriteComradesAsync(IEnumerable<FavouriteComrade> favouriteComrades)
        {
            string favourites = JsonConvert.SerializeObject(favouriteComrades);
            await m_provider.SetFavouritesAsync(favourites);
        }

        private void OnJsonDeserializeError(object sender, Newtonsoft.Json.Serialization.ErrorEventArgs e)
        {
            m_logger.ForErrorEvent()
                .Message("Exception when deserializing json, current object: {currentObject}, error: {error}", e.CurrentObject, e.ErrorContext.Error)
                .Log();
        }
    }
}
