﻿namespace LeftistSupport.Logic.Services
{
    public interface IComradeProvider
    {
        Task<Stream> GetComradesStreamAsync();
        Task<string> GetFavouritesAsync();
        Task SetFavouritesAsync(string favourites);
    }
}
