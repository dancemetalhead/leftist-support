﻿using JsonSubTypes;
using LeftistSupport.Logic.Enums;
using LeftistSupport.Logic.Models;
using LeftistSupport.Logic.Models.SupportWays;
using Newtonsoft.Json;
using NLog;

namespace LeftistSupport.Logic.Services
{
    public sealed class ComradeService
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        private readonly IComradeProvider m_provider;

        public ComradeService(IComradeProvider provider)
        {
            m_provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        public async Task<IList<Comrade>> GetComrades()
        {
            using var reader = new StreamReader(await m_provider.GetComradesStreamAsync());
            var input = await reader.ReadToEndAsync();
            try
            {
                var settings = new JsonSerializerSettings();
                //settings.MissingMemberHandling = MissingMemberHandling.Error; // need to add Type to SupportWay model for this to work, not worth it
                settings.Error += OnJsonDeserializeError;
                settings.Converters.Add(JsonSubtypesConverterBuilder
                    .Of<SupportWay>("Type")
                    .SetFallbackSubtype<OpenUrlSupportWay>() // open url is a default type if "Type" field is not present
                    .RegisterSubtype<OpenUrlSupportWay>(SupportType.OpenUrl)
                    .RegisterSubtype<OpenUrlAndCopySupportWay>(SupportType.OpenUrlAndCopy)
                    .RegisterSubtype<CardSupportWay>(SupportType.Card)
                    .RegisterSubtype<CopyToClipboardSupportWay>(SupportType.CopyToClipboard)
                    .Build());

                var comrades = JsonConvert.DeserializeObject<List<Comrade>>(input, settings);
                return comrades;
            }
            catch (Exception ex)
            {
                m_logger.ForExceptionEvent(ex).Message("Failed to deserialize comrades from provided stream").Log();
                throw new AggregateException("Bad input", ex);
            }
        }

        private void OnJsonDeserializeError(object sender, Newtonsoft.Json.Serialization.ErrorEventArgs e)
        {
            m_logger.ForErrorEvent()
                .Message("Exception when deserializing json, current object: {currentObject}, error: {error}", e.CurrentObject, e.ErrorContext.Error)
                .Log();
        }
    }
}
