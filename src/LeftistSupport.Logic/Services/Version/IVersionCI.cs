﻿namespace LeftistSupport.Logic.Services.Version
{
    public interface IVersionCI
    {
        string CommitShortSha { get; }
        uint BuildNumber { get; }
    }
}
