﻿using LeftistSupport.Logic.Models.Version;

namespace LeftistSupport.Logic.Services.Version
{
    public interface IGitlabRestClient
    {
        Task<GitlabReleaseJson> GetLatestReleaseAsync(CancellationToken cancellationToken);
    }
}
