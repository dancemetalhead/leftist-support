﻿using LeftistSupport.Logic.Models.Version;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NLog;

namespace LeftistSupport.Logic.Services.Version
{
    public sealed class GitlabRestClient : IGitlabRestClient
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        public static string LatestReleaseUrl => "https://gitlab.com/api/v4/projects/44257541/releases/permalink/latest";

        private static HttpClient HttpClient = new ();

        public async Task<GitlabReleaseJson> GetLatestReleaseAsync(CancellationToken cancellationToken = default)
        {
            while (true)
            {
                try
                {
                    var result = await HttpClient.GetStringAsync(LatestReleaseUrl, cancellationToken);

                    return JsonConvert.DeserializeObject<GitlabReleaseJson>(result, new JsonSerializerSettings
                    {
                        ContractResolver = new DefaultContractResolver
                        {
                            NamingStrategy = new SnakeCaseNamingStrategy(),
                        }
                    });
                }
                catch (TaskCanceledException tcex)
                {
                    m_logger.ForWarnEvent().Message($"Get latest version task cancelled: {tcex.Message}").Log();
                    return null;
                }
                catch (HttpRequestException hrex)
                {
                    m_logger.ForExceptionEvent(hrex).Message($"Get latest version requst failed").Log();
                }
                catch (Exception ex)
                {
                    m_logger.ForExceptionEvent(ex).Message($"Get latest version failed").Log();
                    return null;
                }

                try
                {
                    await Task.Delay(30_000, cancellationToken);
                }
                catch { }
            }
        }
    }
}
