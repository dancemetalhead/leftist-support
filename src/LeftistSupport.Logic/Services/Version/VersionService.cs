﻿using LeftistSupport.Logic.Models.Version;
using NLog;
using Semver;

namespace LeftistSupport.Logic.Services.Version
{
    public class VersionService
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        private readonly IVersionCI m_versionCI;
        private readonly IGitlabRestClient m_gitlabRestClient;
        private readonly string m_appVersion;
        private readonly SemVersion m_version;

        public string Version => $"{m_appVersion}+build.{BuildNumber}.{CommitShortSha}";

        public uint BuildNumber => m_versionCI.BuildNumber;

        public string CommitShortSha => m_versionCI.CommitShortSha;

        public VersionService(IVersionTracking versionTracking, IVersionCI versionCI, IGitlabRestClient gitlabRestClient)
        {
            ArgumentNullException.ThrowIfNull(versionTracking, nameof(versionTracking));
            m_versionCI = versionCI ?? throw new ArgumentNullException(nameof(versionCI));
            m_gitlabRestClient = gitlabRestClient ?? throw new ArgumentNullException(nameof(gitlabRestClient));

            m_appVersion = versionTracking.CurrentVersion;
            m_version = SemVersion.Parse(Version, SemVersionStyles.Strict);
        }

        public async Task<AvailableVersion> CheckForNewerVersionAsync(CancellationToken cancellationToken = default)
        {
            var latestVersionResult = await m_gitlabRestClient.GetLatestReleaseAsync(cancellationToken);
            if (latestVersionResult == null)
            {
                m_logger.ForWarnEvent().Message("Couldn't get latest version").Log();
                return null;
            }

            var latestVersion = SemVersion.Parse(latestVersionResult.TagName, SemVersionStyles.Strict);
            if (latestVersion.ComparePrecedenceTo(m_version) < 1)
            {
                m_logger.ForInfoEvent().Message($"Latest version {latestVersion} isn't newer than current {m_appVersion}").Log();
                return null;
            }

            return new AvailableVersion
            {
                Version = latestVersionResult.Name,
                Description = latestVersionResult.Description,
                ReleasedAt = latestVersionResult.ReleasedAt,
                DownloadLink = latestVersionResult.Assets?.Links?.SingleOrDefault(l => l.Name == "leftist-support.apk")?.Url,
            };
        }
    }
}
