﻿namespace LeftistSupport.Logic.Models.Version
{
    public sealed class GitlabReleaseJson
    {
        public string Name { get; set; }
        public string TagName { get; set; }
        public string Description { get; set; }
        public DateTime ReleasedAt { get; set; }
        public GitlabReleaseAssets Assets { get; set; }

        public class GitlabReleaseAssets
        {
            public List<GitlabReleaseAsset> Links { get; set; }
        }

        public class GitlabReleaseAsset
        {
            public string Name { get; set; }
            public string Url { get; set; }
        }
    }
}
