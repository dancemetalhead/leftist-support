﻿namespace LeftistSupport.Logic.Models.Version
{
    public sealed class AvailableVersion
    {
        public string Version { get; set; }
        public string Description { get; set; }
        public DateTime ReleasedAt { get; set; }
        public string DownloadLink { get; set; }
    }
}
