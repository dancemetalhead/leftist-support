﻿using LeftistSupport.Logic.Models.SupportWays;

namespace LeftistSupport.Logic.Models
{
    public sealed class Comrade
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public List<SupportWay> SupportWays { get; set; }
    }
}
