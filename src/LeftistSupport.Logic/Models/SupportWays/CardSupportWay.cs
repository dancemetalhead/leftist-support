﻿namespace LeftistSupport.Logic.Models.SupportWays
{
    public class CardSupportWay : SupportWay
    {
        public new string Icon => "debit_card";
        public string CardNumber { get; set; }
    }
}
