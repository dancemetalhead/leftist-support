﻿namespace LeftistSupport.Logic.Models.SupportWays
{
    public class OpenUrlSupportWay : SupportWay
    {
        public string Url { get; set; }
    }
}
