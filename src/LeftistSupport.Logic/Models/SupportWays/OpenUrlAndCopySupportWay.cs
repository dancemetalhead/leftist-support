﻿namespace LeftistSupport.Logic.Models.SupportWays
{
    public class OpenUrlAndCopySupportWay : OpenUrlSupportWay
    {
        public string CopyToClipboard { get; set; }
    }
}
