﻿namespace LeftistSupport.Logic.Models.SupportWays
{
    public class CopyToClipboardSupportWay : SupportWay
    {
        public string CopyToClipboard { get; set; }
    }
}
