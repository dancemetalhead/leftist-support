﻿namespace LeftistSupport.Logic.Models.SupportWays
{
    public abstract class SupportWay
    {
        public string Icon { get; set; }
        public string Text { get; set; }
    }
}
