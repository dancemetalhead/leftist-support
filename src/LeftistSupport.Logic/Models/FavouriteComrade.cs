﻿namespace LeftistSupport.Logic.Models
{
    public sealed class FavouriteComrade
    {
        public string Title { get; set; }
    }
}
