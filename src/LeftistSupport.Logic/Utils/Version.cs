// 62

using LeftistSupport.Logic.Services.Version;

namespace LeftistSupport.Logic.Utils;

public sealed class Version : IVersionCI
{
    public string CommitShortSha => "c024a12e";

    public uint BuildNumber => 62;
}

