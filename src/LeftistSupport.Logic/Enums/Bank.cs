﻿namespace LeftistSupport.Logic.Enums
{
    public enum Bank
    {
        Unknown,
        Tinkoff,
        Sberbank
    }
}
