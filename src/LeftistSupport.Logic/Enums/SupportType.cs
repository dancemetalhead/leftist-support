﻿namespace LeftistSupport.Logic.Enums
{
    public enum SupportType
    {
        Unknown,
        OpenUrl,
        OpenUrlAndCopy,
        Card,
        CopyToClipboard,
    }
}
