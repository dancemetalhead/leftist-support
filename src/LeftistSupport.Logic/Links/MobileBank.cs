﻿namespace LeftistSupport.Logic.Links
{
    public static class MobileBank
    {
        public static string SberbankOnlineUrl = "https://online.sberbank.ru";
        public static string SberbankPackageNameAndroid = "ru.sberbankmobile";

        public static string TinkoffOnlineUrl = "https://www.tinkoff.ru/payments/card-to-card/";
        public static string TinkoffOnlineAppUri = "tinkoffbank://Main";
    }
}
