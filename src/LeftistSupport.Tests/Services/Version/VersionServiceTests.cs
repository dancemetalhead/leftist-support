using LeftistSupport.Logic.Models.Version;
using LeftistSupport.Logic.Services.Version;
using Moq;

namespace LeftistSupport.Tests.Services.Version
{
    public class VersionServiceTests
    {
        [Test]
        public async Task GetLatestVersion_NewVersionExists_Test()
        {
            // arrange
            var now = DateTime.UtcNow;
            const string newVersion = "0.1.1";
            const string description = "dummy description";
            const string downloadUrl = "https://download.me.com";

            var (versionTracking, versionCI, restClient) = SetupMocks();
            restClient.Setup(rc => rc.GetLatestReleaseAsync(default)).ReturnsAsync(new GitlabReleaseJson
            {
                Name = $"v{newVersion}",
                TagName = newVersion,
                Description = description,
                ReleasedAt = now,
                Assets = new GitlabReleaseJson.GitlabReleaseAssets
                {
                    Links = new List<GitlabReleaseJson.GitlabReleaseAsset>()
                    {
                        new GitlabReleaseJson.GitlabReleaseAsset
                        {
                            Name = "leftist-support.apk",
                            Url = downloadUrl,
                        },
                    },
                },
            });
            var versionService = new VersionService(versionTracking.Object, versionCI.Object, restClient.Object);

            // act
            var result = await versionService.CheckForNewerVersionAsync();

            // assert
            Assert.That(result, Is.Not.Null, "Expected to get newer version");
            Assert.That(result.Version, Is.EqualTo($"v{newVersion}"));
            Assert.That(result.Description, Is.EqualTo(description));
            Assert.That(result.ReleasedAt, Is.EqualTo(now));
            Assert.That(result.DownloadLink, Is.EqualTo(downloadUrl));
        }

        [Test]
        public async Task GetLatestVersion_RequestFailed_Test()
        {
            var (versionTracking, versionCI, restClient) = SetupMocks();
            restClient.Setup(rc => rc.GetLatestReleaseAsync(default)).ReturnsAsync(null as GitlabReleaseJson);

            var versionService = new VersionService(versionTracking.Object, versionCI.Object, restClient.Object);
            
            // act
            var result = await versionService.CheckForNewerVersionAsync();

            // assert
            Assert.That(result, Is.Null);
        }

        [TestCase("0.0.1")]
        [TestCase("0.0.9")]
        [TestCase("0.1.0")]
        [TestCase("0.1.0+234.bcdefghj")]
        public async Task GetLatestVersion_NoNewerVersion_Test(string latestVersion)
        {
            var (versionTracking, versionCI, restClient) = SetupMocks();
            restClient.Setup(rc => rc.GetLatestReleaseAsync(default)).ReturnsAsync(new GitlabReleaseJson
            {
                Name = $"v{latestVersion}",
                TagName = latestVersion,
                Assets = new GitlabReleaseJson.GitlabReleaseAssets
                {
                    Links = new List<GitlabReleaseJson.GitlabReleaseAsset>()
                    {
                        new GitlabReleaseJson.GitlabReleaseAsset
                        {
                            Name = "leftist-support.apk",
                            Url = string.Empty,
                        },
                    },
                },
            });

            var versionService = new VersionService(versionTracking.Object, versionCI.Object, restClient.Object);
            
            // act
            var result = await versionService.CheckForNewerVersionAsync();

            // assert
            Assert.That(result, Is.Null, "Expected null as there's no newer version than the current one");
        }

        private (Mock<IVersionTracking> versionTracking, Mock<IVersionCI> versionCI, Mock<IGitlabRestClient> restClient) SetupMocks()
        {
            var versionTracking = new Mock<IVersionTracking>();
            versionTracking.Setup(v => v.CurrentVersion).Returns("0.1.0");
            var versionCI = new Mock<IVersionCI>();
            versionCI.Setup(vc => vc.BuildNumber).Returns(123);
            versionCI.Setup(vc => vc.CommitShortSha).Returns("abcdefgh");
            var restClient = new Mock<IGitlabRestClient>();
            return (versionTracking, versionCI, restClient);
        }
    }
}