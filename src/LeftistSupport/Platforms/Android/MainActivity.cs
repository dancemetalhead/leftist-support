﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using NLog;
using static Android.Provider.CalendarContract;

namespace LeftistSupport;

[Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
public class MainActivity : MauiAppCompatActivity
{
    private readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

    public static MainActivity Instance { get; private set; }

    public MainActivity()
    {
    }

    protected override void OnCreate(Bundle savedInstanceState)
    {
        base.OnCreate(savedInstanceState);
        Instance = this;
    }

    public async Task ReminderAdd(int day)
    {
        m_logger.ForInfoEvent().Message("Adding reminder on {day} day", day).Log();

        var today = DateTime.Today;
        var eventStartDate = new DateTime(today.Year, today.Month, day, 0, 0, 0, DateTimeKind.Utc);
        var eventEndDate = eventStartDate.AddDays(0);

        var insertCalendarEventIntent = new Intent(Intent.ActionInsert)
            .SetData(Events.ContentUri)
            .PutExtra(Events.InterfaceConsts.Title, "Поддержи левых")
            .PutExtra(ExtraEventBeginTime, new DateTimeOffset(eventStartDate).ToUnixTimeMilliseconds())
            .PutExtra(ExtraEventEndTime, new DateTimeOffset(eventEndDate).ToUnixTimeMilliseconds())
            .PutExtra(ExtraEventAllDay, true)
            .PutExtra(Events.InterfaceConsts.Rrule, "FREQ=MONTHLY")
            .PutExtra(Events.InterfaceConsts.Availability, (int)EventsAvailability.Free);

        StartActivity(insertCalendarEventIntent);

        await Task.CompletedTask;
    }
}
