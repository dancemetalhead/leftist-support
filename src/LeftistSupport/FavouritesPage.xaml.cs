using LeftistSupport.ViewModel;
using LeftistSupport.Views;

namespace LeftistSupport;

public partial class FavouritesPage : ContentPage
{
	public FavouritesPage(FavouritesPageViewModel vm)
	{
		InitializeComponent();
		BindingContext = vm;
	}
}