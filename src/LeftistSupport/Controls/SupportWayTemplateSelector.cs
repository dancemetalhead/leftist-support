﻿using LeftistSupport.ViewModel.SupportWays;

namespace LeftistSupport.Controls
{
    public sealed class SupportWayTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SimpleSupportWayTemplate { get; set; }
        public DataTemplate CardSupportWayTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var supportWayView = (SupportWayViewModel)item;
            return supportWayView switch
            {
                CardSupportWayViewModel => CardSupportWayTemplate,
                _                       => SimpleSupportWayTemplate,
            };
        }
    }
}
