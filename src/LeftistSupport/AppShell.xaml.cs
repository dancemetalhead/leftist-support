﻿namespace LeftistSupport;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();

		Routing.RegisterRoute("comrade_details", typeof(ComradePage));
	}
}
