﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using LeftistSupport.ViewModel;

namespace LeftistSupport.Messages
{
    class FavouriteChangedMessage : ValueChangedMessage<ComradeViewModel>
    {
        public FavouriteChangedMessage(ComradeViewModel value) : base(value)
        {
        }
    }
}
