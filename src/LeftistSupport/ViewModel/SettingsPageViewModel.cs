﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using LeftistSupport.Logic.Models.SupportWays;
using LeftistSupport.Logic.Models.Version;
using LeftistSupport.Services;
using LeftistSupport.ViewModel.SupportWays;
using NLog;

namespace LeftistSupport.ViewModel
{
    public partial class SettingsPageViewModel : ObservableObject
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        public string VersionString => $"Версия приложения: {m_updateCheckerService.CurrentVersion}";

        [ObservableProperty]
        private bool hasNewerVersion;

        public AvailableVersion AvailableVersion => m_updateCheckerService.AvailableVersion;

        public string SourceCodeLink => "https://gitlab.com/dancemetalhead/leftist-support";

        public IList<int> ReminderDatePickerSource { get; }

        public IList<SupportWayViewModel> AuthorSupportWays { get; }

        private const string ReminderDayPreferencesKey = "reminder_day";
        private readonly IPreferences m_preferences;
        private readonly UpdateCheckerService m_updateCheckerService;

        public SettingsPageViewModel(SupportWayViewService supportWayViewService, IPreferences preferences, UpdateCheckerService updateCheckerService)
        {
            ArgumentNullException.ThrowIfNull(supportWayViewService, nameof(supportWayViewService));
            m_preferences = preferences ?? throw new ArgumentNullException(nameof(preferences));
            m_updateCheckerService = updateCheckerService ?? throw new ArgumentNullException(nameof(updateCheckerService));

            ReminderDatePickerSource = Enumerable.Range(1, 31).ToList();

            var supportWays = new List<SupportWay>
            {
                new CardSupportWay
                {
                    Text = "Альфа-банк: 5559 4928 1600 3654",
                    CardNumber = "5559 4928 1600 3654"
                },
                new OpenUrlSupportWay
                {
                    Icon = "email",
                    Text = "Почта для отзывов и предложений: levak.sup@gmail.com",
                    Url = "mailto:levak.sup@gmail.com"
                }
            };
            AuthorSupportWays = supportWayViewService.ConvertToViewModels(supportWays).ToList();
        }

        [RelayCommand]
        public void OnAppearing(Picker dayPicker)
        {
            dayPicker.SelectedIndex = m_preferences.Get(ReminderDayPreferencesKey, 0);

            if (!m_updateCheckerService.HasNewerVersion)
            {
                m_updateCheckerService.OnNewerVersion += OnNewerVersion;
            }
            else
            {
                HasNewerVersion = true;
            }
        }

        [RelayCommand]
        public void OnDisappearing()
        {
            m_updateCheckerService.OnNewerVersion -= OnNewerVersion;
        }

        [RelayCommand]
        public void ReminderDayChanged(int day)
        {
            m_preferences.Set(ReminderDayPreferencesKey, day);
        }

        [RelayCommand]
        public async Task OnReminderAdd(int day)
        {
#if ANDROID
            await MainActivity.Instance.ReminderAdd(day);
#else
            await Task.CompletedTask;
#endif
        }

        [RelayCommand]
        public async Task SupportWayTapped(SupportWayViewModel supportWay)
        {
            m_logger.ForInfoEvent().Message("Tapped on {supportWay} author support way", supportWay).Log();

            await supportWay.OnTapped();
        }

        [RelayCommand]
        public async Task SourceCodeTapped()
        {
            m_logger.ForInfoEvent().Message("Tapped on source code link").Log();

            await Launcher.OpenAsync(SourceCodeLink);
        }

        [RelayCommand]
        public async Task NewVersionTapped()
        {
            m_logger.ForInfoEvent().Message("Tapped on newer version link").Log();

            await DownloadNewerVersion();
        }

        public Task DownloadNewerVersion() => m_updateCheckerService.DownloadNewerVersion();

        private void OnNewerVersion(object sender, UpdateCheckerService.NewerVersionEventArgs e)
        {
            HasNewerVersion = true;
        }
    }
}
