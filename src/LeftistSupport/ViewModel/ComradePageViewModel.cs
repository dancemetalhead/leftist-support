﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using LeftistSupport.ViewModel.SupportWays;
using NLog;

namespace LeftistSupport.ViewModel
{

    [QueryProperty(nameof(Comrade), "Comrade")]
    public partial class ComradePageViewModel : ObservableObject
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        public int DescriptionLinesVisibleByDefault { get; } = 4;

        [ObservableProperty]
        ComradeViewModel comrade;

        [RelayCommand]
        public async Task SupportWayTapped(SupportWayViewModel supportWay)
        {
            m_logger.ForInfoEvent().Message("Tapped on {supportWay} support way", supportWay).Log();

            await supportWay.OnTapped();
        }

        [RelayCommand]
        public void DescriptionTapped(Label label)
        {
            label.MaxLines = label.MaxLines > 0 ? -1 : DescriptionLinesVisibleByDefault;
        }
    }
}
