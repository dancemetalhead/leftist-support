﻿using CommunityToolkit.Mvvm.ComponentModel;
using LeftistSupport.Logic.Enums;

namespace LeftistSupport.ViewModel.SupportWays
{
    public partial class OpenUrlSupportViewModel : SupportWayViewModel
    {
        [ObservableProperty]
        string urlToOpen;

        public override async Task OnTapped()
        {
            bool result = await Launcher.OpenAsync(UrlToOpen);
        }
    }
}
