﻿using CommunityToolkit.Maui.Alerts;

namespace LeftistSupport.ViewModel.SupportWays
{
    public partial class CopyToClipboardSupportWayViewModel : SupportWayViewModel
    {
        private readonly IClipboard m_clipboard;

        protected virtual string TextForClipboard => Text;

        protected virtual string NotificationText => $"Скопировано в буфер обмена: {TextForClipboard}";

        public CopyToClipboardSupportWayViewModel(IClipboard clipboard)
        {
            m_clipboard = clipboard ?? throw new ArgumentNullException(nameof(clipboard));
        }

        public override async Task OnTapped() => await CopyToClipboardAndNotify();

        protected async Task CopyToClipboardAndNotify()
        {
            string clipboardContent = await m_clipboard.GetTextAsync();
            if (clipboardContent == TextForClipboard)
                return;

            await m_clipboard.SetTextAsync(TextForClipboard);

            var toast = Toast.Make(NotificationText);
            await toast.Show();
        }
    }
}
