﻿#if ANDROID
using Android.Content;
#endif
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using LeftistSupport.Logic.Enums;
using LeftistSupport.Logic.Links;

namespace LeftistSupport.ViewModel.SupportWays
{
    public partial class CardSupportWayViewModel : CopyToClipboardSupportWayViewModel
    {
        [ObservableProperty]
        string cardNumber;

        protected override string TextForClipboard => CardNumber;

        protected override string NotificationText => $"Скопирован номер карты {TextForClipboard}";

        public CardSupportWayViewModel(IClipboard clipboard) : base(clipboard)
        {
        }

        [RelayCommand]
        public async Task BankCardTapped(Bank bank)
        {
            await CopyToClipboardAndNotify();

            switch (bank)
            {
                case Bank.Tinkoff:
                    if (!await Launcher.TryOpenAsync(MobileBank.TinkoffOnlineAppUri))
                    {
                        await Launcher.OpenAsync(MobileBank.TinkoffOnlineUrl);
                    }

                    break;

                case Bank.Sberbank:
#if ANDROID
                    var launchIntent = Platform.AppContext.PackageManager.GetLaunchIntentForPackage(MobileBank.SberbankPackageNameAndroid);
                    if (launchIntent != null)
                    {
                        launchIntent.AddCategory(Intent.CategoryLauncher);
                        Platform.AppContext.StartActivity(launchIntent);
                    }
                    else
                    {
                        await Launcher.OpenAsync(MobileBank.SberbankOnlineUrl);
                    }
#else
                    await Launcher.OpenAsync(MobileBank.SberbankOnlineUrl);
#endif

                    break;

                default:
                    throw new NotImplementedException();
            }
        }
    }
}

