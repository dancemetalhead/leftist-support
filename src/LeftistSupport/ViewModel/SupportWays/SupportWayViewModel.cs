﻿using CommunityToolkit.Mvvm.ComponentModel;
using LeftistSupport.Logic.Enums;

namespace LeftistSupport.ViewModel.SupportWays
{
    public partial class SupportWayViewModel : ObservableObject
    {
        [ObservableProperty]
        string iconUrl;

        [ObservableProperty]
        string text;

        public virtual Task OnTapped() => Task.CompletedTask;

        public override string ToString() => $"{GetType().Name} {IconUrl} {Text}";
    }
}
