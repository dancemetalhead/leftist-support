﻿namespace LeftistSupport.ViewModel.SupportWays
{
    public partial class OpenUrlAndCopyToClipboardViewModel : OpenUrlSupportViewModel
    {
        public CopyToClipboardSupportWayViewModel CopyToClipboardViewModel { get; }

        public OpenUrlAndCopyToClipboardViewModel(IClipboard clipboard)
        {
            CopyToClipboardViewModel = new CopyToClipboardSupportWayViewModel(clipboard);
        }

        public override async Task OnTapped()
        {
            await CopyToClipboardViewModel.OnTapped();
            await base.OnTapped();
        }
    }
}
