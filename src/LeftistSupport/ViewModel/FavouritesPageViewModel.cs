﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using LeftistSupport.Messages;
using LeftistSupport.Services;
using NLog;

namespace LeftistSupport.ViewModel
{
    public partial class FavouritesPageViewModel : ComradesBaseViewModel
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        private readonly IMessenger m_messenger;

        [ObservableProperty]
        bool hasNoFavourites;

        [ObservableProperty]
        bool haveSomeFavourites;

        public FavouritesPageViewModel(ComradeViewService comradeViewService, FavouritesViewService favouritesService, IMessenger messenger)
            : base(comradeViewService, favouritesService, messenger)
        {
            m_messenger = messenger ?? throw new ArgumentNullException(nameof(messenger));
            m_messenger.Register<FavouriteChangedMessage>(this, (recipient, f) =>
            {
                UpdateFavouriteState(f.Value);
            });

            m_logger.ForDebugEvent().Message("Created fav page view model").Log();

            hasNoFavourites = true;
            Comrades.CollectionChanged += Favourites_CollectionChanged;
        }

        private void UpdateFavouriteState(ComradeViewModel comrade)
        {
            if (comrade.IsFavourite)
            {
                if (Comrades.All(f => !f.Equals(comrade)))
                    MainThread.BeginInvokeOnMainThread(() => Comrades.Add(comrade));
            }
            else
            {
                if (Comrades.Any(f => f.Equals(comrade)))
                    MainThread.BeginInvokeOnMainThread(() => Comrades.Remove(comrade));
            }
        }

        private void Favourites_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            HasNoFavourites = Comrades.Count == 0;
            HaveSomeFavourites = Comrades.Count > 0;
        }

        [RelayCommand]
        async Task OnAppearing()
        {
            m_logger.ForDebugEvent().Message("Favourites page appearing").Log();

            try
            {
                var comrades = m_comradeService.GetComradesAsync();
                await foreach (var comrade in comrades)
                {
                    UpdateFavouriteState(comrade);
                }
            }
            catch (Exception ex)
            {
                m_logger.ForExceptionEvent(ex).Message("Failed to load comrades").Log();
            }
        }
    }
}
