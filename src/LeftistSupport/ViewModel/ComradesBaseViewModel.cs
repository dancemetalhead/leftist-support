﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using LeftistSupport.Messages;
using LeftistSupport.Services;
using NLog;
using System.Collections.ObjectModel;

namespace LeftistSupport.ViewModel
{
    public abstract partial class ComradesBaseViewModel : ObservableObject
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        protected readonly ComradeViewService m_comradeService;
        protected readonly FavouritesViewService m_favouritesViewService;
        private readonly IMessenger m_messenger;


        [ObservableProperty]
        protected ObservableCollection<ComradeViewModel> comrades;

        protected ComradesBaseViewModel(
            ComradeViewService comradeViewService,
            FavouritesViewService favouritesViewService,
            IMessenger messenger)
        {
            Comrades = new ObservableCollection<ComradeViewModel>();

            m_comradeService = comradeViewService ?? throw new ArgumentNullException(nameof(comradeViewService));
            m_favouritesViewService = favouritesViewService ?? throw new ArgumentNullException(nameof(favouritesViewService));
            m_messenger = messenger ?? throw new ArgumentNullException(nameof(messenger));
        }

        [RelayCommand]
        public async Task OnComradeTapped(ComradeViewModel comrade)
        {
            ArgumentNullException.ThrowIfNull(comrade);

            m_logger.ForInfoEvent().Message("Comrade {comrade} tapped, opening details page", comrade.Title).Log();
            await Shell.Current.GoToAsync("comrade_details", new Dictionary<string, object> { { "Comrade", comrade } });
        }

        [RelayCommand]
        public async Task ToggleFavourite(ComradeViewModel comrade)
        {
            ArgumentNullException.ThrowIfNull(comrade);

            m_logger
                .ForInfoEvent()
                .Message("Comrade {comrade} fav icon tapped, set IsFavourite to {isFavourite}", comrade.Title, !comrade.IsFavourite)
                .Log();

            if (comrade.IsFavourite)
                await m_favouritesViewService.RemoveFromFavouritesAsync(comrade);
            else
                await m_favouritesViewService.AddToFavouritesAsync(comrade);

            m_messenger.Send(new FavouriteChangedMessage(comrade));
        }
    }
}
