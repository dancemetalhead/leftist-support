﻿using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using LeftistSupport.Logic.Models.Version;
using LeftistSupport.Messages;
using LeftistSupport.Services;
using NLog;

namespace LeftistSupport.ViewModel
{
    public partial class MainPageViewModel : ComradesBaseViewModel
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        private readonly IMessenger m_messenger;
        private readonly UpdateCheckerService m_updateCheckerService;

        public Action<AvailableVersion> OnNewerVersionAvailable { get; set; }

        public MainPageViewModel(
            ComradeViewService comradeService,
            FavouritesViewService favouritesService,
            UpdateCheckerService updateCheckerService,
            IMessenger messenger)
            : base(comradeService, favouritesService, messenger)
        {
            m_messenger = messenger ?? throw new ArgumentNullException(nameof(messenger));
            m_messenger.Register<FavouriteChangedMessage>(this, (r, f) =>
            {
                var comradeToUpdate = Comrades.Single(c => c.Equals(f.Value));
                comradeToUpdate.IsFavourite = f.Value.IsFavourite;
            });

            m_updateCheckerService = updateCheckerService ?? throw new ArgumentNullException(nameof(updateCheckerService));

            m_logger.ForDebugEvent().Message("Created main page view model").Log();
        }

        [RelayCommand]
        void OnAppearing()
        {
            m_logger.ForDebugEvent().Message("Main page appearing").Log();

            InitVersionCheck();
            _ = LoadComrades();
        }

        private void InitVersionCheck()
        {
            if (!m_updateCheckerService.HasNewerVersion)
            {
                m_updateCheckerService.OnNewerVersion += OnNewerVersion;
            }
            else
            {
                OnNewerVersion(this, new UpdateCheckerService.NewerVersionEventArgs { AvailableVersion = m_updateCheckerService.AvailableVersion });
            }
        }

        private async Task LoadComrades()
        {
            if (Comrades.Any())
                return;

            try
            {
                m_logger.ForInfoEvent().Message("Loading comrades...").Log();

                var comrades = m_comradeService.GetComradesAsync();
                await foreach (var comrade in comrades)
                {
                    MainThread.BeginInvokeOnMainThread(() => Comrades.Add(comrade));
                }

                m_logger.ForInfoEvent().Message("Loading comrades done").Log();
            }
            catch (Exception ex)
            {
                m_logger.ForExceptionEvent(ex).Message("Failed to load comrades").Log();
            }
        }

        private void OnNewerVersion(object sender, UpdateCheckerService.NewerVersionEventArgs e)
        {
            OnNewerVersionAvailable?.Invoke(e.AvailableVersion);
        }

        public Task DownloadNewerVersion() => m_updateCheckerService.DownloadNewerVersion();
    }
}
