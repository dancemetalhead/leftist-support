﻿using CommunityToolkit.Mvvm.ComponentModel;
using LeftistSupport.ViewModel.SupportWays;
using System.Collections.ObjectModel;

namespace LeftistSupport.ViewModel
{
    public partial class ComradeViewModel : ObservableObject, IEquatable<ComradeViewModel>
    {
        [ObservableProperty]
        string imageUrl;

        [ObservableProperty]
        string title;

        [ObservableProperty]
        string description;

        [ObservableProperty]
        bool isFavourite;

        [ObservableProperty]
        ObservableCollection<SupportWayViewModel> supportWays;

        [ObservableProperty]
        float favIconOpacity;

        [ObservableProperty]
        string favIcon;

        public ComradeViewModel() => UpdateFavProperties(false);

        public bool Equals(ComradeViewModel other)
        {
            return other?.Title == this.Title;
        }

        partial void OnIsFavouriteChanged(bool value) => UpdateFavProperties(value);

        private void UpdateFavProperties(bool value)
        {
            FavIconOpacity = value ? 1f : 0.2f;
            FavIcon = value ? "comrade_fav.png" : "comrade_not_fav.png";
        }
    }
}
