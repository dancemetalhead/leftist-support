using LeftistSupport.ViewModel;

namespace LeftistSupport;

public partial class ComradePage : ContentPage
{
    public ComradePage(ComradePageViewModel vm)
    {
        InitializeComponent();
        BindingContext = vm;
    }
}