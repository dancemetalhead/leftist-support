﻿using LeftistSupport.Logic.Services;

namespace LeftistSupport.Services
{
    internal sealed class ComradeProvider : IComradeProvider
    {

        private const string FavouritesPreferencesKey = "favourites";
        private readonly IPreferences m_preferences;

        public ComradeProvider(IPreferences preferences)
        {
            m_preferences = preferences ?? throw new ArgumentNullException(nameof(preferences));
        }

        public async Task<Stream> GetComradesStreamAsync()
        {
            return await FileSystem.OpenAppPackageFileAsync("comrades.json");
        }

        public Task<string> GetFavouritesAsync()
        {
            return Task.FromResult(m_preferences.Get(FavouritesPreferencesKey, "[]"));
        }

        public Task SetFavouritesAsync(string favourites)
        {
            m_preferences.Set(FavouritesPreferencesKey, favourites);
            return Task.CompletedTask;
        }
    }
}
