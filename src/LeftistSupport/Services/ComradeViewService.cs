﻿using LeftistSupport.Logic.Services;
using LeftistSupport.ViewModel;
using LeftistSupport.ViewModel.SupportWays;
using System.Collections.ObjectModel;

namespace LeftistSupport.Services
{
    public sealed class ComradeViewService
    {
        private readonly ComradeService m_comradeService;
        private readonly FavouritesService m_favouritesService;
        private readonly IServiceProvider m_serviceProvider;
        private readonly SupportWayViewService m_supportWayViewService;

        public ComradeViewService(
            ComradeService comradeService,
            FavouritesService favouritesService,
            IServiceProvider serviceProvider,
            SupportWayViewService supportWayViewService)
        {
            m_comradeService = comradeService ?? throw new ArgumentNullException(nameof(comradeService));
            m_favouritesService = favouritesService ?? throw new ArgumentNullException(nameof(favouritesService));
            m_serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            m_supportWayViewService = supportWayViewService ?? throw new ArgumentNullException(nameof(supportWayViewService));
        }

        public async IAsyncEnumerable<ComradeViewModel> GetComradesAsync()
        {
            var comradesTask = m_comradeService.GetComrades();
            var favouritesTask = m_favouritesService.GetFavouriteComradesAsync();

            await Task.WhenAll(comradesTask, favouritesTask);

            var comradeModels = comradesTask.Result;
            var favourites = favouritesTask.Result;

            foreach (var comradeModel in comradeModels)
            {
                var comrade = m_serviceProvider.GetService<ComradeViewModel>();
                comrade.ImageUrl = $"{comradeModel.Icon}.png";
                comrade.Title = comradeModel.Title;
                comrade.Description = comradeModel.Description;
                comrade.IsFavourite = favourites.Any(f => f.Title == comradeModel.Title);

                var supportWayViewModels = m_supportWayViewService.ConvertToViewModels(comradeModel.SupportWays);
                comrade.SupportWays = new ObservableCollection<SupportWayViewModel>(supportWayViewModels);

                yield return comrade;
            }
        }
    }
}
