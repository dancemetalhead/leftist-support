﻿using LeftistSupport.Logic.Services;
using LeftistSupport.ViewModel;

namespace LeftistSupport.Services
{
    public sealed class FavouritesViewService
    {
        private readonly FavouritesService m_favouritesService;

        public FavouritesViewService(FavouritesService favouritesService)
        {
            m_favouritesService = favouritesService ?? throw new ArgumentNullException(nameof(favouritesService));
        }

        public async Task AddToFavouritesAsync(ComradeViewModel comrade)
        {
            comrade.IsFavourite = true;

            var currentFavourites = await m_favouritesService.GetFavouriteComradesAsync();
            if (currentFavourites.Any(f => f.Title == comrade.Title))
                return;

            currentFavourites.Add(new Logic.Models.FavouriteComrade { Title = comrade.Title });
            await m_favouritesService.SetFavouriteComradesAsync(currentFavourites);
        }

        public async Task RemoveFromFavouritesAsync(ComradeViewModel comrade)
        {
            comrade.IsFavourite = false;

            var currentFavourites = await m_favouritesService.GetFavouriteComradesAsync();
            var comradeToRemove = currentFavourites.FirstOrDefault(f => f.Title == comrade.Title);
            if (comradeToRemove == null)
                return;

            currentFavourites.Remove(comradeToRemove);
            await m_favouritesService.SetFavouriteComradesAsync(currentFavourites);
        }
    }
}
