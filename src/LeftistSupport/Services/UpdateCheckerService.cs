﻿using LeftistSupport.Logic.Models.Version;
using LeftistSupport.Logic.Services.Version;
using NLog;

namespace LeftistSupport.Services
{
    public sealed class UpdateCheckerService
    {
        private static readonly ILogger m_logger = LogManager.GetCurrentClassLogger();

        private readonly VersionService m_versionService;

        public string CurrentVersion => m_versionService.Version;

        public AvailableVersion AvailableVersion { get; private set; }

        public bool HasNewerVersion => AvailableVersion != null;

        public bool AlreadyNotified { get; set; }

        public class NewerVersionEventArgs : EventArgs
        {
            public AvailableVersion AvailableVersion { get; set; }
        }

        public event EventHandler<NewerVersionEventArgs> OnNewerVersion;

        public UpdateCheckerService(VersionService versionService)
        {
            m_versionService = versionService ?? throw new ArgumentNullException(nameof(versionService));

            Task.Run(CheckForNewerVersionAsync);
        }

        public async Task DownloadNewerVersion()
        {
            if (!HasNewerVersion)
                throw new InvalidOperationException("No new version available");

            await Launcher.OpenAsync(AvailableVersion.DownloadLink);
        }

        private async Task CheckForNewerVersionAsync()
        {
            m_logger.ForInfoEvent().Message("Starting query for newer version...").Log();

            AvailableVersion = await m_versionService.CheckForNewerVersionAsync();

            if (AvailableVersion == null)
            {
                m_logger.ForInfoEvent().Message("Query for newer version completed. There's no newer version.").Log();
                return;
            }

            m_logger.ForInfoEvent().Message("Query for newer version completed. Newer version: " +
                $"{AvailableVersion.Version} {AvailableVersion.ReleasedAt}").Log();
            OnNewerVersion(this, new NewerVersionEventArgs { AvailableVersion = AvailableVersion });
        }
    }
}
