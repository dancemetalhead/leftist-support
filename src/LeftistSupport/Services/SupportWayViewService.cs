﻿using LeftistSupport.Logic.Models.SupportWays;
using LeftistSupport.ViewModel.SupportWays;

namespace LeftistSupport.Services
{
    public sealed class SupportWayViewService
    {
        private readonly IServiceProvider m_serviceProvider;

        public SupportWayViewService(IServiceProvider serviceProvider)
        {
            m_serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public IEnumerable<SupportWayViewModel> ConvertToViewModels(IEnumerable<SupportWay> supportWays)
            => supportWays.Select(ToViewModel);

        public SupportWayViewModel ToViewModel(SupportWay supportWay)
        {
            switch (supportWay)
            {
                case OpenUrlAndCopySupportWay openUrlAndCopySupportWay:
                    var openUrlAndCopyViewModel = m_serviceProvider.GetService<OpenUrlAndCopyToClipboardViewModel>();
                    openUrlAndCopyViewModel.IconUrl = $"{openUrlAndCopySupportWay.Icon}.png";
                    openUrlAndCopyViewModel.UrlToOpen = openUrlAndCopySupportWay.Url;
                    openUrlAndCopyViewModel.Text = openUrlAndCopySupportWay.Text ?? openUrlAndCopySupportWay.Url;
                    openUrlAndCopyViewModel.CopyToClipboardViewModel.Text = openUrlAndCopySupportWay.CopyToClipboard;
                    return openUrlAndCopyViewModel;

                case OpenUrlSupportWay openUrlSupportWay:
                    var openUrlViewModel = m_serviceProvider.GetService<OpenUrlSupportViewModel>();
                    openUrlViewModel.IconUrl = $"{openUrlSupportWay.Icon}.png";
                    openUrlViewModel.UrlToOpen = openUrlSupportWay.Url;
                    openUrlViewModel.Text = openUrlSupportWay.Text ?? openUrlSupportWay.Url;
                    return openUrlViewModel;

                case CardSupportWay cardSupportWay:
                    var cardViewModel = m_serviceProvider.GetService<CardSupportWayViewModel>();
                    cardViewModel.IconUrl = $"{cardSupportWay.Icon}.png";
                    cardViewModel.Text = cardSupportWay.Text;
                    cardViewModel.CardNumber = cardSupportWay.CardNumber;
                    return cardViewModel;

                case CopyToClipboardSupportWay copyToClipboardSupportWay:
                    var copyViewModel = m_serviceProvider.GetService<CopyToClipboardSupportWayViewModel>();
                    copyViewModel.IconUrl = $"{copyToClipboardSupportWay.Icon}.png";
                    copyViewModel.Text = copyToClipboardSupportWay.Text;
                    return copyViewModel;

                default:
                    throw new NotImplementedException($"Unknown type {supportWay.GetType()}");
            }
        }
    }
}
