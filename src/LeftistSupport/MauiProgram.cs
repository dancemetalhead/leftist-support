﻿using LeftistSupport.ViewModel;
using CommunityToolkit.Maui;
using LeftistSupport.Logic.Services;
using LeftistSupport.Services;
using LeftistSupport.ViewModel.SupportWays;
using NLog;
using LeftistSupport.Exceptions;
using Microsoft.Maui.LifecycleEvents;
#if ANDROID
using System.Reflection;
#endif
using CommunityToolkit.Mvvm.Messaging;
using Sharpnado.CollectionView;
using LeftistSupport.Logic.Services.Version;

namespace LeftistSupport;

public static class MauiProgram
{
	private static readonly ILogger Logger;

	static MauiProgram()
	{
#if ANDROID
		Logger = LogManager
			.Setup()
			.LoadConfigurationFromAssemblyResource(typeof(App).GetTypeInfo().Assembly)
			.GetCurrentClassLogger();
#else
		Logger = LogManager.GetCurrentClassLogger();
#endif
	}

	public static MauiApp CreateMauiApp()
	{
		Logger.ForInfoEvent().Message("Creating MAUI app...").Log();

		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.UseMauiCommunityToolkit()
			.UseSharpnadoCollectionView(loggerEnable: false, debugLogEnable: false)
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
			})
			.ConfigureLifecycleEvents(events =>
			{
#if ANDROID
				events.AddAndroid(android => android.OnStop((activity) => OnAppQuit()));
#elif IOS
				events.AddiOS(ios => ios.WillTerminate((app) => OnAppQuit()));
#elif WINDOWS
				events.AddWindows(windows => windows.OnClosed((window, args) => OnAppQuit()));
#endif
			}).ConfigureEssentials(essentials =>
            {
                essentials.UseVersionTracking();
            });

		builder.Services.AddSingleton(Clipboard.Default)
			.AddSingleton(Preferences.Default)
			.AddSingleton<IMessenger>(StrongReferenceMessenger.Default)
			.AddSingleton(VersionTracking.Default);

		builder.Services.AddSingleton<MainPage>()
            .AddSingleton<MainPageViewModel>()
			.AddTransient<ComradePage>()
			.AddTransient<ComradePageViewModel>()
			.AddSingleton<SettingsPage>()
			.AddSingleton<SettingsPageViewModel>()
			.AddSingleton<FavouritesPage>()
			.AddSingleton<FavouritesPageViewModel>();

		builder.Services
			.AddTransient<ComradeViewModel>()
			.AddTransient<OpenUrlSupportViewModel>()
			.AddTransient<CopyToClipboardSupportWayViewModel>()
			.AddTransient<OpenUrlAndCopyToClipboardViewModel>()
			.AddTransient<CardSupportWayViewModel>();

		builder.Services
			.AddSingleton<IComradeProvider, ComradeProvider>()
			.AddSingleton<ComradeService>()
			.AddSingleton<ComradeViewService>()
			.AddSingleton<SupportWayViewService>()
			.AddSingleton<FavouritesService>()
			.AddSingleton<FavouritesViewService>()
			.AddSingleton<VersionService>()
			.AddSingleton<IVersionCI, Logic.Utils.Version>()
			.AddSingleton<IGitlabRestClient, GitlabRestClient>()
			.AddSingleton<UpdateCheckerService>();

		MauiExceptions.UnhandledException += CurrentDomain_UnhandledException;
        TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;

		Logger.ForInfoEvent().Message("Configured app and registered services").Log();

		return builder.Build();
	}

    private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
		Logger.ForExceptionEvent(e.ExceptionObject as Exception).Message("Unhandled exception: {exception}", e.ExceptionObject).Log();
    }

    private static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
    {
		e.SetObserved();
		var ex = e.Exception;
		Logger.ForExceptionEvent(ex).Message("Unhandled task exception").Log();
    }

	private static void OnAppQuit()
	{
		Logger.ForInfoEvent().Message("App is closing, shutdown logging").Log();
		LogManager.Shutdown();
	}
}
