﻿using LeftistSupport.Logic.Models.Version;
using LeftistSupport.ViewModel;

namespace LeftistSupport;

public partial class MainPage : ContentPage
{
	private readonly MainPageViewModel viewModel;

	public MainPage(MainPageViewModel vm)
	{
		InitializeComponent();
		viewModel = vm;
		BindingContext = vm;

		vm.OnNewerVersionAvailable = OnNewerVersionAvailable;
	}

    private void OnNewerVersionAvailable(AvailableVersion availableVersion)
    {
		string description = $"{availableVersion.Description}{Environment.NewLine}" +
			$"Загрузить новую версию {availableVersion.Version}" +
			$" от {availableVersion.ReleasedAt.ToShortDateString()}?";

		Task.Run(() => MainThread.InvokeOnMainThreadAsync(
				 () => DisplayAlert("Обновление", description, "Да", "Нет"))
			)
			.ContinueWith(async t =>
			{
				if (!t.Result)
					return;

				await viewModel.DownloadNewerVersion();
			});
    }
}